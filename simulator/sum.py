import sys
from random import expovariate, gauss, uniform
from user import *
from simulator import *
import matplotlib

import decimal

# Generator
def drange(start, stop, step):
    i = 0
    while start + i * step < stop:
        yield start + i * step
        i += 1

stats = {}
stats['node'] = list()
stats['d'] = list()
stats['f'] = list()
stats['sum'] = list()
stats['frequency'] = list()

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 12}

matplotlib.rc('font', **font)


def printHelp():
    print("usage ./function <number_of_iterations>")
    quit()


if(len(sys.argv) == 2):
    nIterations = int(sys.argv[1])
    print("Running ",  nIterations, " iterations")
else:
    printHelp()
    quit()




for d100 in range(1, 100, 1):
    d = d100/100 # generate a contineous d from 0 to 1

    #create users
    #create 4 users for each d value 0 - 1 (100 steps)
    #different f = different amount of reward
    users = {}
    users[0] = User(0, 0, 0, d=d, f=1, frequency = 1)
    users[1] = User(1, 0, 0, d=d, f=10, frequency = 1)
    users[2] = User(2, 0, 0, d=d, f=100, frequency = 1)
    users[3] = User(3, 0, 0, d=d, f=1000, frequency = 1)


    #set prestige to static values
    for user in users.values():
        user.prestige = user.staticValue()

    #nIterations = number of blocks: sum amount of prestige gained
    for i in range(0, nIterations):
        for user in users.values():
            user.update()


    #gather stats
    for user in users.values():
        stats['node'].append(user.id)
        stats['d'].append(user.d)
        stats['f'].append(user.f)
        stats['sum'].append(user.sum - user.staticValue())
        stats['frequency'].append(user.frequency)

print(stats)
df = pd.DataFrame(stats)
fig, ax = plt.subplots()

for key, group in df.groupby('node'):
    styles = ['solid', 'dashdot', 'dashed', 'dotted', 'solid']
    bx = group.plot('d', 'sum', ax=ax, legend=True, linewidth=2.5, linestyle=styles[key]);
    bx.set_xlabel("d")
    bx.set_ylabel("Prestige Sum over Static Value")
    bx.set_yscale('log')

plt.legend(labels=['prestige gained/block = 1', 'prestige gained/block = 10', 'prestige gained/block = 100', 'prestige gained/block = 1000'])
plt.show()
