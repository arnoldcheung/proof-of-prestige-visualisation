import sys
import copy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import networkx as nx

from simulator import *
from user import *
import misc

def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy

def getNodeSize():
    global DG, users
    return [users[node].prestige/size_divider for node in list(DG)]

'''
def staticSize():
    global DG, users
    return [users[node].staticValue()/size_divider for node in list(DG)]
'''
    
def getNodeColour():
    global DG, users, traversed, recently_traversed
    colours = []
    '''
    for user, staticValue in zip(users.values(), staticValues):
        if user.prestige < 0.98 * staticValue:
            colours.append('green')
        elif 0.98 * staticValue <= user.prestige <= 1.02 * staticValue:
            colours.append('cyan')
        else:
            colours.append('red')
    return colours
    '''
    #print('colour traversed nodes: {}'.format(traversed))
    for node in list(DG):
        if node in recently_traversed:
            colours.append('green')
        
        elif node in traversed:
            colours.append('cyan')
        
        else:
            colours.append('red')
    #print('colours: {}'.format(colours))
    return colours

# Updates annotation per click
def update_annot(ind):
    user_index = ind["ind"][0]
 
    text = "User {}\nCois: {}\nDecay: {}\nStatic Value: {}\nPrestige: {}".format(list(DG)[user_index],
                                                                                 round(users[list(DG)[user_index]]._coins, 1),
                                                                                 users[list(DG)[user_index]]._d,
                                                                                 labels_static[user_index],
                                                                                 round(users[list(DG)[user_index]]._prestige, 1))
    annot.set_text(text)

# On-click annotation control
def on_click(event):
    global ind, cont, clicked
    if event.inaxes == ax:
        cont, ind = node_borders.contains(event)
        if cont:
            clicked = True
            update_annot(ind)
            annot.set_visible(True)
            fig.canvas.draw_idle()
        else:
            annot.set_visible(False)
            fig.canvas.draw_idle()

def updatefig(*args):
    #global prestige_nodes, edges, edge_labels, DG, traversed, roots, edgelist, recently_traversed, edgeLabelList
    global prestige_nodes, edges, edge_labels, traversed, roots, edgelist, recently_traversed, edgeLabelList, ind
    
    prestige_nodes.remove()
    
    if edges is not None:
        while edges:
            edge = edges.pop(0)
            edge.remove()
            del edge
    
    '''
    if edge_labels is not None:
        while edge_labels:
            label = edge_labels.pop(0)
            label.remove()
            del label
    '''

    #static_nodes = nx.draw_networkx(DG, pos, node_size=staticSize(), with_labels=True, edgelist=edgelist, node_color='grey')
    #node_borders = nx.draw_networkx_nodes(DG, pos, node_size=staticSizes, node_color='w', edgecolors='k', linewidths=0.5, alpha=0.5)
    prestige_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getNodeSize(), node_color=getNodeColour(), alpha=0.4)
    #node_labels = nx.draw_networkx_labels(DG, pos, font_size=9)

    #static_edges = nx.draw_networkx_edges(DG, pos, alpha=0.1)
    edges = nx.draw_networkx_edges(DG, pos, edgelist=edgelist)

    #edge_labels = nx.draw_networkx_edge_labels(DG, pos, edge_labels=edgeLabelList, ax=ax)

    ax.set_xlim([-1.2, 1.2])
    ax.set_ylim([-1.2, 1.2])
    ax.set_xticks([])
    ax.set_yticks([])
    

    new_transfers_edges = []
    new_transfers_nodes = []
    new_transfers_amount = {}
    
    for user in users.values():
        user.update()

    root_gen = (root_node for root_node in roots if not all(x in traversed for x in DG.successors(root_node)))
    for root_node in root_gen:
        #print('root_node: {}'.format(root_node))
        
        successor_gen = (successor_node for successor_node in DG.successors(root_node) if successor_node not in traversed)
        for successor_node in successor_gen:
            #print('successor_node: {}'.format(successor_node))
            
            if users[successor_node].isWorking():
                print('Node: {} distributes to Node: {}'.format(root_node, successor_node))

                # prestige transfer
                users[root_node].prestige += misc.f
                users[successor_node].prestige -= misc.f
                

                new_transfers_edges.append((successor_node, root_node))
                new_transfers_nodes.append(successor_node)
                new_transfers_amount[(successor_node, root_node)] = misc.f
                
                edgelist = copy.copy(new_transfers_edges)
                recently_traversed = copy.copy(new_transfers_nodes)
                edgeLabelList = copy.copy(new_transfers_amount)
                
                traversed.append(successor_node)

    roots = copy.copy(traversed)

    if clicked:
        if len(ind["ind"]):
            update_annot(ind)

############################################################################################################################################

# figure configurations
fig, ax = plt.subplots(figsize=(16,8))
fig.suptitle('Simple Mining Simulation')
ax.set_xlim([-1.2, 1.2])
ax.set_ylim([-1.2, 1.2])
ax.axis('off')


x = 0
n_nodes = 50
iterations = 200
size_divider = 5

# Graph (network) and users separated
DG = generateRandomTree(n_nodes)
users = generateUsersCoinPrestige(n_nodes, 500, 0, 20) #coin, prestige, work_prob(%)


# calculates static value for each user
staticValues = [users[node].staticValue() for node in list(DG)]

# scales node size based on static value
staticSizes = [staticValue / size_divider for staticValue in staticValues]

# rounded off values
labels_static = [round(staticValue, 1) for staticValue in staticValues]

roots, paths = find_paths(DG)
traversed = copy.copy(roots)

edgelist = []
recently_traversed = []
edgeLabelList= {}

'''
print(paths)

print('successors')

for node in DG:
    print(list(DG.successors(node)))
'''

#pos = nx.random_layout(DG, center=[0,0])
pos = nx.fruchterman_reingold_layout(DG, k=0.7, scale=1.12)
#pos = nx.spring_layout(DG, k=0.5)
#pos = graphviz_layout(DG)
#pos = nx.nx_agraph.graphviz_layout(DG, prog='dot')
#pos = nx.circular_layout(DG, scale = 2)
#pos = nx.shell_layout(DG, scale = 1.05)

#static_nodes = nx.draw_networkx(DG, pos, node_size=staticSize(), with_labels=True, edgelist=edgelist, node_color='grey')
node_borders = nx.draw_networkx_nodes(DG, pos, node_size=staticSizes, node_color='w', edgecolors='k', linewidths=0.5, alpha=0.5)
prestige_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getNodeSize(), node_color=getNodeColour(), alpha=0.4)
node_labels = nx.draw_networkx_labels(DG, pos, font_size=9)

static_edges = nx.draw_networkx_edges(DG, pos, alpha=0.1)
edges = nx.draw_networkx_edges(DG, pos, edgelist=edgelist)
#edge_labels = nx.draw_networkx_edge_labels(DG, pos, edge_labels=edgeLabelList, ax=ax)


# initialise annotation
## overheads
ind = None
cont = False
clicked = False
pause = False
auto = False
counter = -1

annot = ax.annotate("", xy=(-1.2,-1.2), fontsize='medium', fontweight='ultralight', bbox=dict(boxstyle="round", fc="w"))
annot.set_visible(False)

# connects event to canvas
cid1 = fig.canvas.mpl_connect("button_press_event", on_click)

ani = animation.FuncAnimation(fig, updatefig, frames=iterations, interval=50)

plt.show()

