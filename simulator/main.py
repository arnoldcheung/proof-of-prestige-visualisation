import sys
from random import expovariate, gauss, uniform
from user import *
from simulator import *




def printHelp():
    print("usage ./coins <number_of_users> <number_of_iterations>")
    quit()


if(len(sys.argv) == 3):
    nUsers = int(sys.argv[1])
    nIterations = int(sys.argv[2])
    print("Generating ",  nUsers,  " users and running ",  nIterations, " iterations")
else:
    printHelp()



if(len(sys.argv) < 2):
    print("usage ./app <graph_size> <number_of_iterations>")
    print("or ./app <filename>")
    quit()

if(len(sys.argv) == 2):
    readFile(sys.argv[1])


if(len(sys.argv) == 3):
    size = sys.argv[1] 
    iterations = int(sys.argv[2])
    print("Generating a random graph with size " + size)

    users = generateUsersPrestige(size, 100)

    nIterations = 0#int(uniform(10, 50))
    for i in range(0, iterations):

        print("Running ", nIterations, " iterations ")
        for i in range(0, nIterations):
            #Perform iteration
            for user in users.values():
                user.update()
            generateUserStats(users)

        #print(users_coins)
        print(users_prestige)
        #printUsersStats(users)
        #quit()

        DG = generateRandomTree(size)
        calculatePrestigeSimple(users, DG)
        generateUserStats(users, 0)

        #distribute_payment(DG, 1000)

        #print("Nodes after: ")
        #for user in users.values():
        #    print(user)


        #generateStats(users, DG)

    #printStats()
    printUsersStats(users)

