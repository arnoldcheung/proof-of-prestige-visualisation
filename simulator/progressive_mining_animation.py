import sys
import copy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import networkx as nx

from simulator import *
from user import *
import misc
from hierarchy_layout import *

def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy

def getStaticValues():
    return [users[node].staticValue() for node in list(DG)]

def getStaticNodeSize():
    global DG, users, size_divider
    staticValues = getStaticValues()
    return [staticValue / size_divider for staticValue in staticValues]

def getPrestigeNodeSize():
    global DG, users
    return [users[node].prestige/size_divider for node in list(DG)]
    
def getNodeColour():
    global DG, users, traversed, recently_traversed, miner
    colours = []
    '''
    for user, staticValue in zip(users.values(), staticValues):
        if user.prestige < 0.98 * staticValue:
            colours.append('green')
        elif 0.98 * staticValue <= user.prestige <= 1.02 * staticValue:
            colours.append('cyan')
        else:
            colours.append('red')
    return colours
    '''
    #print('colour traversed nodes: {}'.format(traversed))
    for node in list(DG):
        if miner == node:
            colours.append('yellow')
        
        elif node in recently_traversed:
            colours.append('green')
        
        elif node in traversed:
            colours.append('cyan')
        
        else:
            colours.append('red')
    #print('colours: {}'.format(colours))
    return colours

def get_mining_prob():
    global DG, users, total_prestige, n_nodes
    total_prestige = sum(user.prestige for user in users.values())
    if total_prestige == 0:
        return [1/n_nodes for node in list(DG)]
    else:
        return [users[node].prestige/total_prestige for node in list(DG)]

# Updates annotation per click
def update_annot(ind):
    user_index = ind["ind"][0]
    text = "User {}\nCoins: {}\nStatic Value: {}\nPrestige: {}\nMining Probability: {}%\nPrestige Retained: {}\nTotal Retained: {}".format(list(DG)[user_index],
                                                                                 round(users[list(DG)[user_index]]._annot_coins, 1),
                                                                                 round(getStaticValues()[user_index], 1),
                                                                                 round(users[list(DG)[user_index]]._annot_prestige, 1),
                                                                                 round(annot_mining_prob[user_index]*100, 3),
                                                                                 round(users[list(DG)[user_index]]._annot_gain, 2),
                                                                                 round(users[list(DG)[user_index]]._annot_work_gain, 2))
                                                                                 
    annot = ax.annotate(text, xy=annot_pos, fontsize='medium', fontweight='ultralight', bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)

# On-click annotation control
def on_click(event):
    global ind, cont, clicked
    if event.inaxes == ax:
        cont, ind = static_nodes.contains(event)
        if cont:
            clicked = True
            update_annot(ind)
            annot.set_visible(True)
            fig.canvas.draw_idle()
        else:
            annot.set_visible(False)
            fig.canvas.draw_idle()

# on-press spacebar pause/unpause
def on_press(event):
    global pause
    if event.key.isspace():
        pause ^= True
        if pause:
            pause_annot = ax.annotate("Paused", xy=pause_pos, fontsize='xx-large', fontweight='medium', bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)
            annot.set_visible(True)
            fig.canvas.draw_idle()
        else:
            annot.set_visible(False)
            fig.canvas.draw_idle()

    elif event.key == 'r':
        restart_animation()

def restart_animation():
    global users, saved_users, n_nodes, DG, traversed, roots, edgelist, recently_traversed, edgeLabelList, block_id
    
    print('**********Restart**********')
    
    #for user in users.values():
    #user.prestige = 0
        
    users = copy.deepcopy(saved_users)
    
    roots, paths = find_paths(DG)
    traversed = copy.copy(roots)
    print(traversed)

    edgelist = []
    recently_traversed = []
    edgeLabelList= {}
    block_id = 0

    ##### network redrawn #####
    ax.clear()

    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.axis('off')

    static_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getStaticNodeSize(), node_color='w', edgecolors='k', linewidths=0.5, alpha=0.5)
    prestige_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getPrestigeNodeSize(), node_color=getNodeColour(), alpha=0.4)
    node_labels = nx.draw_networkx_labels(DG, pos, font_size=9)

    static_edges = nx.draw_networkx_edges(DG, pos, alpha=0.1)
    edges = nx.draw_networkx_edges(DG, pos, edgelist=edgelist)
    edge_labels = nx.draw_networkx_edge_labels(DG, pos, edge_labels=edgeLabelList, font_size=8)

    annot = ax.annotate("", xy=annot_pos, fontsize='medium', fontweight='ultralight', bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)
    annot.set_visible(False)


def updatefig(*args):
    #global prestige_nodes, edges, edge_labels, DG, traversed, roots, edgelist, recently_traversed, edgeLabelList
    global traversed, roots, edgelist, recently_traversed, edgeLabelList, ind, miner, annot_mining_prob, block_reward, block_id

    if not pause:
        ax.clear()

        #static_nodes = nx.draw_networkx(DG, pos, node_size=staticSize(), with_labels=True, edgelist=edgelist, node_color='grey')
        static_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getStaticNodeSize(), node_color='w', edgecolors='k', linewidths=0.5, alpha=0.5)
        prestige_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getPrestigeNodeSize(), node_color=getNodeColour(), alpha=0.4)
        node_labels = nx.draw_networkx_labels(DG, pos, font_size=9)

        static_edges = nx.draw_networkx_edges(DG, pos, alpha=0.1)
        edges = nx.draw_networkx_edges(DG, pos, edgelist=edgelist)
        edge_labels = nx.draw_networkx_edge_labels(DG, pos, edge_labels=edgeLabelList, font_size=8)
        
        for user in users.values():
            user.annot_coins = user.coins
            user.annot_prestige = user.prestige
            user.annot_gain = user.gained
            user.annot_work_gain = user.work_gain
            user.gained = 0
        
        annot_mining_prob = get_mining_prob()
        
        if clicked:
            if len(ind["ind"]):
                update_annot(ind)
        
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.axis('off')

        new_transfers_edges = []
        new_transfers_nodes = []
        new_transfers_amount = {}

        miner = np.random.choice(list(DG), p=get_mining_prob())
        users[miner].coins += block_reward
        
        print('\n[----Block {}---miner:{}---reward: {}-----]'.format(block_id, miner, block_reward))

        root_gen = (root_node for root_node in roots if not all(x in traversed for x in DG.successors(root_node)))
        for root_node in root_gen:
            #print('root_node: {}'.format(root_node))
            
            successor_gen = (successor_node for successor_node in DG.successors(root_node) if successor_node not in traversed)
            for successor_node in successor_gen:
                #print('successor_node: {}'.format(successor_node))
                
                if users[successor_node].isWorking() and (users[successor_node].prestige >= misc.f):
                    print('\nNode {} distributes to Node {}'.format(root_node, successor_node))

                    # prestige transfer
                    
                    ### new progressive section ###
                    current_node = copy.copy(successor_node)
                    pred = list(DG.predecessors(current_node))
                    prestige_transit = 0
                    
                    while(pred):
                        path_before = nx.shortest_path(DG, 0, pred[0])
                        path_strength = calculatePathStrengthSum(users, path_before)
                        node_strength = calculateNodeStrength(users, current_node)
                    
                        info = animationCalculateTransfer(current_node, node_strength, path_strength, prestige_transit, traversed)
                        prestige_transit = info['transfer']
                        users[current_node].gained += info['gained'] - info['lost']
                        #users[current_node].prestige += users[current_node].gained
                        
                        sn = current_node
                        rn = pred[0]
                        new_transfers_edges.append((sn, rn))
                        if (sn, rn) in new_transfers_amount:
                            new_transfers_amount[(sn, rn)] += round(prestige_transit, 2)
                        else:
                            new_transfers_amount[(sn, rn)] = round(prestige_transit, 2)
                        
                        #print('{} transfers {} upstream'.format(sn, prestige_transit))
                        
                        current_node = pred[0]
                        pred = list(DG.predecessors(current_node))
                        
                    #print("Adding ", prestige_transit, "to root ", current_node)
                    users[current_node].gained += prestige_transit
                    
                    #new_transfers_edges.append((successor_node, root_node))
                    new_transfers_nodes.append(successor_node)
                    #new_transfers_amount[(successor_node, root_node)] = misc.f
                    
                    edgelist = copy.copy(new_transfers_edges)
                    recently_traversed = copy.copy(new_transfers_nodes)
                    edgeLabelList = copy.copy(new_transfers_amount)
                    
                    traversed.append(successor_node)

        roots = copy.copy(traversed)
        
        block_id += 1
        
        for user in users:
            users[user].prestige += users[user].gained
            users[user].work_gain += users[user].gained
        
        for user in users.values():
            user.update()


############################################################################################################################################

# figure configurations
fig, ax = plt.subplots(figsize=(16,7.5))
fig.suptitle('Progressive Mining Simulation')
xlim = [0, 1]
ylim = [-1, 0.1]
annot_pos = [xlim[0]-0.1, ylim[1]-0.1]
pause_pos = [xlim[1], ylim[1]]

n_nodes = 50
iterations = 200
size_divider = 6
block_reward = 50

# Graph (network) and users separated
DG = generateRandomTree(n_nodes)
users = generateUsersCoinPrestige(n_nodes, min_coin=20, max_coin=500, max_prestige=0, work_prob=20) #coin, prestige, work_prob(%)

# Save users for restart: same amount of starting coins as it is otherwise randomised
saved_users = copy.deepcopy(users)

total_prestige = sum(user.prestige for user in users.values())
print(total_prestige)
print(list(DG))
print(get_mining_prob())

# calculates static value for each user
#staticValues = [users[node].staticValue() for node in list(DG)]

# scales node size based on static value
#staticSizes = [staticValue / size_divider for staticValue in staticValues]

# rounded off values
#labels_static = [round(staticValue, 1) for staticValue in staticValues]

roots, paths = find_paths(DG)
traversed = copy.deepcopy(roots)

edgelist = []
recently_traversed = []
edgeLabelList= {}
miner = None


##### Test node positions #####

#pos = nx.random_layout(DG, center=[0,0])
#pos = nx.fruchterman_reingold_layout(DG, center=[0.5, -0.55], scale=0.5)
#pos = nx.pygraphviz_layout(G, prog = 'dot')
pos = hierarchy_pos(DG, 0)
#pos = nx.spring_layout(DG, k=0.5)
#pos = graphviz_layout(DG)
#pos = nx.nx_agraph.graphviz_layout(DG, prog='dot')
#pos = nx.circular_layout(DG, scale = 2)
#pos = nx.shell_layout(DG, scale = 1.05)


static_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getStaticNodeSize(), node_color='w', edgecolors='k', linewidths=0.5, alpha=0.5)
prestige_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getPrestigeNodeSize(), node_color=getNodeColour(), alpha=0.4)
node_labels = nx.draw_networkx_labels(DG, pos, font_size=9)

static_edges = nx.draw_networkx_edges(DG, pos, alpha=0.1)
edges = nx.draw_networkx_edges(DG, pos, edgelist=edgelist)
edge_labels = nx.draw_networkx_edge_labels(DG, pos, edge_labels=edgeLabelList, font_size=8)

annot = ax.annotate("", xy=annot_pos, fontsize='medium', fontweight='ultralight', bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)
annot.set_visible(False)

# initialise annotation
## overheads
ind = None
cont = False
clicked = False
pause = False
auto = False
block_id = 0



# connects event to canvas
cid1 = fig.canvas.mpl_connect("button_press_event", on_click)
cid2 = fig.canvas.mpl_connect("key_press_event", on_press)

ani = animation.FuncAnimation(fig, updatefig, frames=iterations, interval=1000)

plt.show()

