import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import networkx as nx

from simulator import *
from user import *

def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy

def getNodeSize():
    global users
    sizes = [user._prestige/size_divider for user in users.values()]
    for user in users.values():
        user.update()
    return sizes


def updatefig(*args):
    ax.clear()
    static_nodes = nx.draw_networkx(DG, pos, with_labels=True, width=0.5, node_size=staticSize, node_color='b')
    prestige_nodes = nx.draw_networkx_nodes(DG, pos, with_labels=False, width=0.5, node_size=getNodeSize())
    ax.set_xticks([])
    ax.set_yticks([])

fig, ax = plt.subplots(figsize=(12,8))

x = 0
n_nodes = 20
iterations = 200
size_divider = 3

users = generateUsersCoin(n_nodes, 300)
staticSize = [user.staticValue()/size_divider for user in users.values()]
DG = generateRandomTree(n_nodes)


pos = nx.fruchterman_reingold_layout(DG, k=0.8)
#pos = nx.spring_layout(DG, k=3)

ani = animation.FuncAnimation(fig, updatefig, frames=iterations, interval=300)
plt.show()
