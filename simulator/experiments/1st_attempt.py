import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import networkx as nx

def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy

def f(x):
    return np.sin(x) + 2

def updatefig(*args):
    ax.clear()
    global x
    G = nx.draw_networkx(DG, pos, node_size = 100 * f(x))
    x += np.pi / 10
    ax.set_xticks([])
    ax.set_yticks([])

fig, ax = plt.subplots(figsize=(6,4)) 
x = 0
DG = generateRandomTree(10)
pos = nx.random_layout(DG)
ani = animation.FuncAnimation(fig, updatefig, interval=10)
plt.show()
