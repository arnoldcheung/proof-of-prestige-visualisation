import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import networkx as nx

from simulator import *
from user import *

def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy

def getNodeSize():
    global users
    for user in users.values():
        user.update()
    return [user._prestige/2 for user in users.values()]


def updatefig(*args):
    ax.clear()
    static_nodes = nx.draw_networkx_nodes(DG, pos, with_labels=False, width=0.5, node_size=staticSize, node_color='b')
    prestige_nodes = nx.draw_networkx(DG, pos, with_labels=False, width=0.5, node_size=getNodeSize())
    ax.set_xticks([])
    ax.set_yticks([])

fig, ax = plt.subplots(figsize=(12,8))

x = 0
n_nodes = 20
iterations = 100

users = generateUsersCoinPrestige(n_nodes, 100, 0)
staticSize = [user.staticValue()/2 for user in users.values()]
DG = generateRandomTree(n_nodes)


pos = nx.fruchterman_reingold_layout(DG)
#pos = nx.random_layout(DG)

ani = animation.FuncAnimation(fig, updatefig, frames=iterations, interval=100)
plt.show()
