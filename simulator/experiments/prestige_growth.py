import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import networkx as nx
from functools import partial
# import mpld3

from simulator import *
from user import *

def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy

def getNodeSize():
    global users
    sizes = [user._prestige/size_divider for user in users.values()]
    return sizes

def getNodeColour():
    global users
    colours = []
    for user, staticValue in zip(users.values(), staticValues):
        if user._prestige <= 0.9 * staticValue:
            colours.append('green')
        else:
            colours.append('red')
    return colours

def updatefig(*args):
    ax.clear()
    #static_nodes = nx.draw_networkx(DG, pos, with_labels=True, width=0.5, node_size=staticSize, node_color='grey')
    static_nodes = nx.draw_networkx_nodes(G, pos, with_labels=False, node_size=staticSizes, node_color='grey')
    prestige_nodes = nx.draw_networkx_nodes(G, pos, with_labels=False, node_size=getNodeSize(), node_color=getNodeColour(), alpha=0.4)
    
    #print(type(static_nodes))
    
    for user in users.values():
        user.update()
    
    annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                        bbox=dict(boxstyle="round", fc="w"),
                        arrowprops=dict(arrowstyle="->"))

    annot.set_visible(False)
    
    def update_annot(ind):
        annot_pos = static_nodes.get_offsets()[ind["ind"][0]]
        annot.xy = annot_pos
        text = "here"
        annot.set_text(text)

    def hover(event):
        global i
        vis = annot.get_visible()
        if event.inaxes == ax:
            cont, ind = static_nodes.contains(event)
            
            # test
            print(i)
            i += 1
            
            if cont:
                update_annot(ind)
                annot.set_visible(True)
                fig.canvas.draw_idle()
            else:
               if vis:
                     annot.set_visible(False)
                     fig.canvas.draw_idle()

    '''
    annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                        bbox=dict(boxstyle="round", fc="w"),
                        arrowprops=dict(arrowstyle="->"))

    annot.set_visible(True)
    '''
    #hover_partial = partial(hover, plot=static_nodes)
    fig.canvas.mpl_connect("motion_notify_event", hover)

    ax.set_xlim([0, 3])
    ax.set_ylim([0, 3])
    #ax.set_xticks([])
    #ax.set_yticks([])
    #fig.patch.set_visible(False)
    ax.axis('off')

fig, ax = plt.subplots(figsize=(6,4))

# xn_nodes = 15
iterations = 1000000

# control node size
size_divider = 0.5

# users = generateUsersCoinPrestige(n_nodes, 500, 0)
users = {}
users[0] = User(0, 80, 0, d=0.05)
users[1] = User(1, 80, 0, d=0.15)
users[2] = User(0, 50, 0, d=0.05)
users[3] = User(1, 50, 0, d=0.15)

# test i
i = 0

staticValues = [user.staticValue() for user in users.values()]
staticSizes = [staticValue / size_divider for staticValue in staticValues]
labels = [str(staticValue) for staticValue in staticValues]
print(labels)

# DG = generateRandomTree(n_nodes)
# Graph initialising
G = nx.Graph()
G.add_nodes_from(users)

#pos = nx.fruchterman_reingold_layout(G)
#pos = nx.spring_layout(DG, k=3)
#pos = nx.random_layout(G)
pos = {0: (2, 1), 1: (2, 2), 2: (1, 1), 3: (1, 2)}

ani = animation.FuncAnimation(fig, updatefig, frames=iterations, interval=100, repeat=False)

fig.suptitle('The Growth of Prestige Over Time')
plt.show()

