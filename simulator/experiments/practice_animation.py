import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import networkx as nx

fig, ax = plt.subplots(figsize=(6,4))

def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy

x = 0

def f(x):
    return np.sin(x) + 2

DG = generateRandomTree(10)
pos = nx.random_layout(DG)

def updatefig(*args):
    plt.clf()
    global x
    G = nx.draw_networkx(DG, pos, node_size = 100 * f(x))
    x += np.pi / 10
    ax.set_xticks([])
    ax.set_yticks([])

ani = animation.FuncAnimation(fig, updatefig, interval=50)
plt.show()
