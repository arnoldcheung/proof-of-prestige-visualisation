import random

class User:

    def __init__(self, id, coins = 0, prestige = 0, d = 0.1, f = 10, frequency = 0, work_prob = 0):
        self._id = id
        self._coins = coins
        self._prestige = prestige
        self.gained = 0
        self._d = d
        self._sum = 0
        self._f = f
        self._counter = 0
        self._frequency = frequency
        self._work_prob = work_prob
        self._offset = 0
        #### annotation ####
        self._annot_coins = coins
        self._annot_prestige = prestige
        self._annot_gain = 0
        self._work_gain = 0
        self._annot_work_gain = 0

    @property
    def prestige(self):
        return self._prestige
    @prestige.setter
    def prestige(self, value):
        self._prestige = value


    @property
    def gained(self):
        return self._gained
    @gained.setter
    def gained(self, value):
        self._gained = value

    @property
    def frequency(self):
        return self._frequency
    @property
    def id(self):
        return self._id
    @property
    def sum(self):
        return self._sum

    @property
    def f(self):
        return self._f

    @property
    def coins(self):
        return self._coins

    @coins.setter
    def coins(self, value):
        self._coins = value

    def isWorking(self):
        result =  (random.randint(0,100) < self._work_prob)
        if((result == True) & (self._offset == 0)):
            return True
        elif((result == True) & (self._offset > 0)) :
            self._offset -= 1
            return False
        else:
            return False

    def addOffset(self, inc):
        self._offset += inc


    @property
    def d(self):
        return self._d

    @d.setter
    def d(self, value):
        self._d = value

    ##### annotation #####
    @property
    def annot_coins(self):
        return self._annot_coins
    
    @annot_coins.setter
    def annot_coins(self, value):
        self._annot_coins = value
    
    @property
    def annot_prestige(self):
        return self._annot_prestige

    @annot_prestige.setter
    def annot_prestige(self, value):
        self._annot_prestige = value
    
    @property
    def annot_gain(self):
        return self._annot_gain
    
    @annot_gain.setter
    def annot_gain(self, value):
        self._annot_gain = value
    
    @property
    def work_gain(self):
        return self._work_gain
    
    @work_gain.setter
    def work_gain(self, value):
        self._work_gain = value
    
    @property
    def annot_work_gain(self):
        return self._annot_work_gain
    
    @work_gain.setter
    def annot_work_gain(self, value):
        self._annot_work_gain = value

    
    # Prestige gained when done work
    # Prestige decay
    def update(self):
        if(self._frequency > 0):
            if ((self._counter % self._frequency) == 0):
                self._prestige += self._f
            #print("Adding ", self._f, " prestige.")

        self._counter += 1
        self._sum += self._prestige
        self._prestige += self._coins - (self._prestige * self._d)
        #print("Prestige after decay: ", self._prestige)


    def staticValue(self):
        #print("static value:", self._coins / self._d)
        return (self._coins / self._d)

    def __str__(self):
        return "User: " + str(self.id) + ", coins: " + str(self._coins) + ", prestige: " + str(self.prestige) + ", gained: " + str(self.gained) + ", sum: " + str(self._sum)
