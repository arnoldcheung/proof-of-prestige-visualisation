import networkx as nx
import matplotlib.pyplot as plt
from matplotlib import lines
import sys
from itertools import chain
from itertools import product
from itertools import starmap
from functools import partial
from random import expovariate, gauss, uniform
from itertools import groupby
from user import *
import pandas as pd
import matplotlib
import misc

###############################
# Structures to gather stats  #
###############################
stats = {}
stats['node'] = list()
stats['d'] = list()
stats['f'] = list()
stats['sum'] = list()
stats['frequency'] = list()


#############################
# Font settings for graphs  #
#############################
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 12}
matplotlib.rc('font', **font)


traversed = set()
###################################################
# calculate the amount of prestige sent upstream  #
###################################################
def calculateTransfer(node, node_strength, path_strength, prestige_transit):
    #fee paid by the node
    #The node is added to traversed to pay only once
    print(traversed)
    if(node not in traversed):
        node_loses = misc.f
        traversed.add(node)
    else:
        node_loses = 0

    # The retain function, user retains some value from downstream and the rest is sent upstream
    node_gains = (prestige_transit * node_strength) / (path_strength + node_strength)
    print("Initial transit: ", prestige_transit)
    print("Node ", node, " looses: ", node_loses, " Node gains: ", node_gains)
    transfered_up = (prestige_transit + node_loses - node_gains)
    print("Transfered up: ", transfered_up)
    return {'lost': node_loses,
            'gained': node_gains,
            'transfer': transfered_up}


################################################
# calculate path strength using max function   #
################################################
def calculatePathStrengthMax(users, path):
    max_prestige = 0
    for node in path:
        if(users[node].prestige > max_prestige):
            max_prestige = users[node].prestige

    return max_prestige

################################################
# calculate path strength using sum function   #
################################################
def calculatePathStrengthSum(users, path):
    sum_prestige = 0
    for node in path:
        sum_prestige += users[node].prestige

    return sum_prestige

#############################
# calculate node strength   #
#############################
def calculateNodeStrength(users, node):
    return users[node].prestige * 2

#################################
# SimpleMining implementation   #
#################################
def calculatePrestigeSimple(users, G):
    traversed.clear()
    #get roots and leaves to find all the paths in the tree we have to traverse
    chaini = chain.from_iterable
    roots = (v for v, d in G.in_degree() if d == 0)
    leaves = (v for v, d in G.out_degree() if d == 0)
    all_paths = partial(nx.all_simple_paths, G)
    found_paths = list(chaini(starmap(all_paths, product(roots, leaves))))

    #Traverse the paths we've found
    for path in found_paths:
        print("Found path: ", path)
        prestige_sum = 0
        node = path[-1] #the end beneficiary of the path
        print("Node: ", node)
        pred = list(G.predecessors(node)) #the contributor of the leaf
        print("Predescessor: ", pred)
        prestige_transit = 0
        #go node by node along the path and calculate prestige transfers
        while(pred):
            #if we've more than 1 predescessor, the tree is incorrect
            if(len(pred) > 1):
                print("More than 1 predescessor")
                quit()
            #move prestige around
            if(node not in traversed):
                 users[node].prestige -= misc.f
                 users[pred[0]].prestige += misc.f #pred[0] is int, an index to the user number
                 print("Moving ", misc.f, " prestige from", node, "to", pred[0])
                 traversed.add(node)

            node = pred[0]
            # move up the path
            pred = list(G.predecessors(node))

    return;


####################################
# ProgressiveMining implementation #
####################################
def calculatePrestige(users, G):
    traversed.clear()
    #get roots and leaves to find all the paths in the tree we have to traverse
    chaini = chain.from_iterable
    roots = (v for v, d in G.in_degree() if d == 0)
    leaves = (v for v, d in G.out_degree() if d == 0)
    all_paths = partial(nx.all_simple_paths, G)
    found_paths = list(chaini(starmap(all_paths, product(roots, leaves))))

    #Traverse the paths we've found
    for path in found_paths:
        print("Found path: ", path)
        prestige_sum = 0
        node = path[-1]
        print("Node: ", node)
        pred = list(G.predecessors(node))
        print("Predescessor: ", pred)
        prestige_transit = 0
        #go node by node along the path and calculate prestige transfers
        while(pred):
            #if we've more than 1 predescessor, the tree is incorrect
            if(len(pred) > 1):
                print("More than 1 predescessor")
                quit()
            #calculate path strength, not the most efficient, but flexible and easy to write
            #calculating nx.shortest_path all the time can be a bottleneck
            path_before = nx.shortest_path(G, 0, pred[0])
            print("Path predescessing: ", path_before)
            path_strength = calculatePathStrengthSum(users, path_before)
            node_strength = calculateNodeStrength(users, node)

            #move prestige around
            print("Node ", node, " Node's strength: ", node_strength, "Path strength: ", path_strength)
            info = calculateTransfer(node, node_strength, path_strength, prestige_transit)
            prestige_transit = info['transfer']
            users[node].gained += info['gained'] - info['lost']
            node = pred[0]
            pred = list(G.predecessors(node))
        #add prestige to the root
        print("Adding ", prestige_transit, "to root ", node)
        users[node].gained += prestige_transit

    for user in users:
        users[user].prestige += users[user].gained
        users[user].gained = 0

    return;


def generateUsersPrestige(size, max_prestige):
    #Generate users and allocates money and prestige
    users = {}
    for i in range(0, int(size)):
        #don't put 0 prestige on the nodes, it's used for error checking
        users[i] = User(i, 0, uniform(10, max_prestige))
        #(self, id, coins = 0, prestige = 0, d = 0.1, f = 10, frequency = 0, work_prob = 0):
        print(users[i])

    return users

users_coins = {}
users_prestige = {}
users_stats_counter = -1



#######################################
# Statistics section                  #
# Should be moves to a different file #
#######################################

# tracks user amount of coins and prestige
def generateUserStats(users, inc = 1):
    global users_stats_counter
    users_stats_counter += inc
    print(users_stats_counter)
    if(users_stats_counter == 0): # initialise list in the beginning
        for user in users:
            users_coins[user] = list()
            users_prestige[user] = list()
    # add new amount of coins and presitge to list
    for user in users:
        print("Counter: ", users_stats_counter)
        users_coins[user].append((users_stats_counter, users[user].coins))
        users_prestige[user].append((users_stats_counter, users[user].prestige))




def printUsersStats(users):
    global users_stats_counter
    for user in users:
        keys = list([i[0] for i in users_prestige[user]])
        # i[0] is the first element of users_prestige[user] list
        # i.e. the counter number
        vals = list([i[1] for i in users_prestige[user]])
        # i[1] is the second element of users_prestige[user] list
        # i.e. the amount of prestige
        user_label = "C=" + str(users[user].coins) + ", d=" + str(users[user].d) # C = coins, d = decay parameter
        styles = ['dashed', 'dashdot', 'dashdot', 'dotted', 'solid']
        plt.plot(keys, vals, label=user_label, linewidth=2.2, linestyle=styles[user])
        plt.plot((0, users_stats_counter), (users[user].staticValue(), users[user].staticValue()), 'k-', linestyle='dashed')

    plt.xlabel('Time[blocks]')
    plt.ylabel('Prestige')
    #plt.title("Prestige evolution over time")
    plt.legend()
    plt.show()

    for user in users:
        keys = list([i[0] for i in users_coins[user]])
        # same as above but for coins
        vals = list([i[1] for i in users_coins[user]])
        # same as above but for coins
        user_label = "user " + str(user)
        plt.plot(keys, vals, label=user_label)

    plt.xlabel('Time[blocks]')
    plt.ylabel('Coins')
    plt.title("Coins evolution over time")
    plt.legend()
    #plt.show()

stats = {}
stats['node'] = list()
stats['distance'] = list()
stats['transfers'] = list()
stats['prestige'] = list()
stats['gain'] = list()
stats['type'] = list()
stats['iter'] = list()

def generateStats(orig_users, users, G, type, iter):

    paths = nx.single_source_shortest_path_length(G, 0)
    for node in G.nodes:
        stats['node'].append(node)
        stats['distance'].append(paths[node])
        stats['transfers'].append(G.out_degree(node))
        stats['prestige'].append(orig_users[node].prestige)
        stats['gain'].append(users[node].prestige  - orig_users[node].prestige)
        stats['type'].append(type)
        stats['iter'].append(iter)




def printStats():
    df = pd.DataFrame(stats)
    print(stats)

    fig, ax = plt.subplots()
    for key, group in df.groupby('type'):
        avg = group.groupby('transfers')['gain'].mean()
        std = group.groupby('transfers')['gain'].std()
        ax.set_xlabel("ax label")
        bx = avg.plot(x='transfers', y='gain', yerr=std, ax=ax, legend=True);
        bx.set_xlabel("Number of performed services")
        bx.set_ylabel("Prestige gained")

    plt.legend(labels=['Progressive', 'Simple'])
    #plt.show()

    fig, ax = plt.subplots()
    for key, group in df.groupby('type'):
        avg = group.groupby('distance')['gain'].mean()
        std = group.groupby('distance')['gain'].std()
        bx =avg.plot(x='distance', y='gain', yerr=std, ax=ax, legend=True);
        bx.set_xlabel("Distance from root")
        bx.set_ylabel("Prestige gained")


    plt.legend(labels=['Progressive', 'Simple'])

    fig, ax = plt.subplots()
    for key, group in df.groupby('type'):
        bx = group.plot('prestige', 'gain', style='o', ax=ax, legend=True);
        bx.set_xlabel("Base prestige")
        bx.set_ylabel("Prestige gained")

    plt.legend(labels=['Progressive', 'Simple'])
    plt.show()



#read a topology from a one simulator traces file
def readFile(filename):
    print("Reading file " + filename)
    trees = {}
    roots = {}
    delimiter = " "
    f = open(filename,"r")
    for line in f:
        if line.startswith("#"):
            continue
        source = line.split(delimiter)[2]
        destination = line.split(delimiter)[3].rstrip()
        origin = line.split(delimiter)[1].split("-")[2]
        tree = line.split(delimiter)[1][0:-len(origin)]

        if "pp2-1-" not in line:
            continue
        print(line)
        print("Got source:", source, " destination: ", destination, " origin: ", origin, " tree: ", tree)

        #Create a new graph if necessary
        if(tree not in trees):
            trees[tree] = nx.DiGraph()

        #Add the edge to the graph
        trees[tree].add_edge(source, destination)

        #Record the origins for each tree
        if(tree not in roots):
            roots[tree] = set()
        roots[tree].add(origin)
    f.close()

    print("Found ", len(trees), " trees:")
    for tree in trees:
        DG = trees[tree]
        #Add the publisher as root to all the trees
        for origin in roots[tree]:
            DG.add_edge(0, origin)
        print("Tree ", tree, " with ", len(DG.nodes), " nodes")
        if "pp2-1-" not in tree:
            print("Skipping")
            continue
        print("Nodes: ")
        counter=0
        for node in DG.nodes:
            counter += 1
            print(counter," ", node)

        print("Edges: ")
        counter=0
        for edge in DG.edges:
            counter += 1
            print(counter," ", edge)
        allocatePrestige(DG)
        calcuatePrestige(DG)
        generateStats(DG)

    printStats()
    quit()

#Generate a random graph
def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy

#######################################
# Arnold added section                #
#######################################

def generateUsersCoinPrestige(size, min_coin, max_coin, max_prestige, work_prob):
    #Generate users and allocates money and prestige
    users = {}
    for i in range(0, int(size)):
        #don't put 0 prestige on the nodes, it's used for error checking
        users[i] = User(i, uniform(min_coin, max_coin), uniform(0, max_prestige), work_prob = work_prob)
        #(self, id, coins = 0, prestige = 0, d = 0.1, f = 10, frequency = 0, work_prob = 0):
        print(users[i])
    
    return users

def find_paths(G):
    #get roots and leaves to find all the paths in the tree we have to traverse
    chaini = chain.from_iterable
    roots = [v for v, d in G.in_degree() if d == 0]
    leaves = (v for v, d in G.out_degree() if d == 0)
    all_paths = partial(nx.all_simple_paths, G)
    found_paths = list(chaini(starmap(all_paths, product(roots, leaves))))
    return roots, found_paths

def animationCalculateTransfer(node, node_strength, path_strength, prestige_transit, traversed):
    #fee paid by the node
    #The node is added to traversed to pay only once
    if(node not in traversed):
        node_loses = misc.f
    else:
        node_loses = 0

    # The retain function, user retains some value from downstream and the rest is sent upstream
    node_gains = (prestige_transit * node_strength) / (path_strength + node_strength)
    print("Initial transit: ", prestige_transit)
    print("Node ", node, " looses: ", node_loses, " Node gains: ", node_gains)
    transfered_up = (prestige_transit + node_loses - node_gains)
    print("Transfered up: ", transfered_up)
    return {'lost': node_loses, 'gained': node_gains, 'transfer': transfered_up}


