import sys
import copy

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.ticker import MaxNLocator
import networkx as nx

from simulator import *
from user import *
import misc
from hierarchy_layout import *

##### Directed Graph Generation #####
def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy

##### Node visualisation #####
def getStaticValues():
    return [users[node].staticValue() for node in list(DG)]

def getStaticNodeSize():
    global DG, users
    staticValues = getStaticValues()
    return [staticValue / misc.size_divider for staticValue in staticValues]

def getPrestigeNodeSize():
    global DG, users
    return [users[node].prestige / misc.size_divider for node in list(DG)]
    
def getNodeColour():
    global DG, users, traversed, recently_traversed, miner
    colours = []
    
    #print('colour traversed nodes: {}'.format(traversed))
    for node in list(DG):
        if miner == node:
            colours.append('yellow')
        
        elif node in recently_traversed:
            colours.append('green')
        
        elif node in traversed:
            colours.append('cyan')
        
        else:
            colours.append('None')
    #print('colours: {}'.format(colours))
    return colours

def getLargeNodeColour(node):
    colours = []
    
    if node.prestige < 0.98 * node.staticValue():
        colours.append('green')
    elif 0.98 * node.staticValue() <= node.prestige <= 1.02 * node.staticValue():
        colours.append('blue')
    else:
        colours.append('red')

    return colours

##### Mining Probability #####
def get_mining_prob():
    global DG, users, total_prestige
    total_prestige = sum(user.prestige for user in users.values())
    if total_prestige == 0:
        return [1/n_nodes for node in list(DG)]
    else:
        return [users[node].prestige/total_prestige for node in list(DG)]

##### Statistics for graph (ax2) #####
def generateStats(users, inc = 1):
    global counter
    
    counter += inc
    
    if(counter == 0): # initialise list in the beginning
        for user in users:
            users_coins[user] = list()
            users_prestige[user] = list()

    # add recent amount of coins and presitge to list
    for user in users:
        users_coins[user].append((counter, users[user].coins))
        users_prestige[user].append((counter, users[user].prestige))

##### update functions (ax2, 3, 4)#####
def update_annot(ind):

    user_index = ind["ind"][0]
    text = "User {}\nCoins: {}\nStatic Value: {}\nPrestige: {}\nMining Probability: {}%\nPrestige Retained: {}\nTotal Retained: {}".format(list(DG)[user_index],
                                                                                                                           round(users[list(DG)[user_index]]._annot_coins, 1),
                                                                                                                           round(getStaticValues()[user_index], 1),
                                                                                                                           round(users[list(DG)[user_index]]._annot_prestige, 1),
                                                                                                                           round(annot_mining_prob[user_index]*100, 3),
                                                                                                                           round(users[list(DG)[user_index]]._annot_gain, 2),
                                                                                                                           round(users[list(DG)[user_index]]._annot_work_gain, 2))
                                                                                                                       
    annot = ax4.annotate(text, xy=annot_pos, fontsize='medium', fontweight='ultralight', bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)

def update_graph(ind):
    global users, users_coins, users_prestige
    
    ax2.clear()
    
    user_index = ind["ind"][0]
    
    current_static = getStaticValues()[user_index]
    
    keys = list([i[0] for i in users_prestige[list(DG)[user_index]]])
    vals = list([i[1] for i in users_prestige[list(DG)[user_index]]])
    
    ax2.plot(keys, vals, linewidth=1)
    
    ax2.axhline(y=getStaticValues()[user_index], color='k', linestyle='dashed', linewidth=1) #current static in black
    ax2.axhline(y=starting_static[user_index], color='r', linestyle='dashed', linewidth=1) # initial static in red
    
    ax2.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax2.set_yticks(np.arange(0, np.around(current_static * 1.5, decimals=-2), 100 if current_static * 1.5 < 1000 else 1000 if current_static * 1.5 > 5000 else 500))
    
    ax2.grid(b=True)
    ax2.set_xlabel('Time[blocks]')
    ax2.set_ylabel('Prestige')

    ax2.set_ylim([0, current_static * 1.5])

def update_node(ind):
    
    ax3.clear()
    ax3.axis('off')
    
    user_index = ind["ind"][0]
    largeNode = users[list(DG)[user_index]]

    G = nx.Graph()
    G.add_node(largeNode)
    
    largeStaticSize = [largeNode.staticValue() * 4]
    largePrestigeSize = [largeNode.prestige * 4]

    largeStaticNode = nx.draw_networkx_nodes(G, pos={largeNode:(1,1)}, node_size=largeStaticSize, node_color='w', edgecolors='k', linewidths=0.5, alpha=0.5, ax=ax3)
    largePrestigeNode = nx.draw_networkx_nodes(G, pos={largeNode:(1,1)}, node_size=largePrestigeSize, node_color=getLargeNodeColour(largeNode), alpha=0.4, ax=ax3)

##### event handling #####
def on_click(event):
    global ind, cont, clicked
    if event.inaxes == ax1:
        cont, ind = static_nodes.contains(event)
        if cont:
            clicked = True
            
            update_annot(ind)
            update_graph(ind)
            update_node(ind)
            
            annot.set_visible(True)
            fig.canvas.draw_idle()
        else:
            annot.set_visible(False)
            
            ax2.clear()
            ax3.clear()
            ax4.clear()
            
            ax2.axis('off')
            ax3.axis('off')
            ax4.axis('off')
            
            fig.canvas.draw_idle()

def on_press(event):
    global pause
    if event.key.isspace():
        pause ^= True
        if pause:
            pause_annot = ax1.annotate("Paused", xy=pause_pos, fontsize='xx-large', fontweight='medium', bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)
            annot.set_visible(True)
            fig.canvas.draw_idle()
        else:
            annot.set_visible(False)
            fig.canvas.draw_idle()

    elif event.key == 'r':
        restart_animation()

##### restart animation #####
def restart_animation():
    global users, saved_users, n_nodes, DG, traversed, roots, edgelist, recently_traversed, edgeLabelList, block_id, counter, users_coins, users_prestige, pause
    
    print('**********Restart**********')
    
    ##### axis cleared #####
    ax1.clear()
    ax2.clear()
    ax3.clear()
    ax4.clear()
        
    users = copy.deepcopy(saved_users)
    
    roots, paths = find_paths(DG)
    traversed = copy.copy(roots)

    # ax2 graph variables
    users_coins = {}
    users_prestige = {}
    counter = -1

    edgelist = []
    recently_traversed = []
    edgeLabelList= {}
    block_id = 0

    ##### network redrawn #####
    ax1.set_xlim(xlim)
    ax1.set_ylim(ylim)
    ax1.axis('off')
    ax2.axis('off')
    ax3.axis('off')
    ax4.axis('off')
    
    if not pause:
        
        static_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getStaticNodeSize(), node_color='w', edgecolors='k', linewidths=0.5, alpha=0.5)
        prestige_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getPrestigeNodeSize(), node_color=getNodeColour(), alpha=0.4)
        node_labels = nx.draw_networkx_labels(DG, pos, font_size=9)
        
        if show_network_edges:
            static_edges = nx.draw_networkx_edges(DG, pos, alpha=0.1)
        edges = nx.draw_networkx_edges(DG, pos, edgelist=edgelist)
        edge_labels = nx.draw_networkx_edge_labels(DG, pos, edge_labels=edgeLabelList, ax=ax1)

        annot = ax4.annotate("", xy=annot_pos, fontsize='medium', fontweight='ultralight', bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)
        annot.set_visible(False)


def simple_mining(*args):
    global traversed, roots, edgelist, recently_traversed, edgeLabelList, ind, miner, annot_mining_prob, block_id, users_coins, users_prestige

    if not pause:
        ax1.clear()
        ax2.clear()
        ax3.clear()
        ax4.clear()

        ax1.set_xlim(xlim)
        ax1.set_ylim(ylim)
        ax1.axis('off')
        ax2.axis('off')
        ax3.axis('off')
        ax4.axis('off')
        
        static_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getStaticNodeSize(), node_color='w', edgecolors='k', linewidths=0.5, alpha=0.5, ax=ax1)
        prestige_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getPrestigeNodeSize(), node_color=getNodeColour(), alpha=0.4, ax=ax1)
        node_labels = nx.draw_networkx_labels(DG, pos, font_size=9, ax=ax1)
        
        if show_network_edges:
            static_edges = nx.draw_networkx_edges(DG, pos, alpha=0.1, ax=ax1)
        edges = nx.draw_networkx_edges(DG, pos, edgelist=edgelist, ax=ax1)
        edge_labels = nx.draw_networkx_edge_labels(DG, pos, edge_labels=edgeLabelList, font_size=8, ax=ax1)
        
        for user in users.values():
            user.annot_coins = user.coins
            user.annot_prestige = user.prestige
            user.annot_gain = user.gained
            user._annot_work_gain = user.work_gain
            user.gained = 0
        
        annot_mining_prob = get_mining_prob()
        
        generateStats(users)

        if clicked:
            if len(ind["ind"]):
                update_annot(ind)
                update_graph(ind)
                update_node(ind)

        new_transfers_edges = []
        new_transfers_nodes = []
        new_transfers_amount = {}

        miner = np.random.choice(list(DG), p=get_mining_prob())
        users[miner].coins += misc.block_reward
        
        print('\n[----Block {}---miner:{}---reward: {}-----]'.format(block_id, miner, misc.block_reward))
        
        root_gen = (root_node for root_node in roots if not all(x in traversed for x in DG.successors(root_node)))
        for root_node in root_gen:
            #print('root_node: {}'.format(root_node))
            
            successor_gen = (successor_node for successor_node in DG.successors(root_node) if successor_node not in traversed)
            for successor_node in successor_gen:
                #print('successor_node: {}'.format(successor_node))
                
                if users[successor_node].isWorking() and (users[successor_node].prestige >= misc.f):
                    print('Node: {} distributes to Node: {}'.format(root_node, successor_node))

                    # prestige transfer
                    users[root_node].gained += misc.f
                    users[successor_node].gained -= misc.f
                    
                    new_transfers_edges.append((successor_node, root_node))
                    new_transfers_nodes.append(successor_node)
                    new_transfers_amount[(successor_node, root_node)] = misc.f
                    
                    edgelist = copy.copy(new_transfers_edges)
                    recently_traversed = copy.copy(new_transfers_nodes)
                    edgeLabelList = copy.copy(new_transfers_amount)
                    
                    traversed.append(successor_node)

        roots = copy.copy(traversed)
        
        block_id += 1

        for user in users:
            users[user].prestige += users[user].gained
            users[user].work_gain += users[user].gained

        for user in users.values():
            user.update()

def progressive_mining(*args):
    global traversed, roots, edgelist, recently_traversed, edgeLabelList, ind, miner, annot_mining_prob, block_id
    
    if not pause:
        ax1.clear()
        ax2.clear()
        ax3.clear()
        ax4.clear()
        
        ax1.set_xlim(xlim)
        ax1.set_ylim(ylim)
        ax1.axis('off')
        ax2.axis('off')
        ax3.axis('off')
        ax4.axis('off')
        
        static_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getStaticNodeSize(), node_color='w', edgecolors='k', linewidths=0.5, alpha=0.5, ax=ax1)
        prestige_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getPrestigeNodeSize(), node_color=getNodeColour(), alpha=0.4, ax=ax1)
        node_labels = nx.draw_networkx_labels(DG, pos, font_size=9, ax=ax1)
       
        if show_network_edges:
            static_edges = nx.draw_networkx_edges(DG, pos, alpha=0.1, ax=ax1)
        edges = nx.draw_networkx_edges(DG, pos, edgelist=edgelist, ax=ax1)
        edge_labels = nx.draw_networkx_edge_labels(DG, pos, edge_labels=edgeLabelList, font_size=8, ax=ax1)
        
        for user in users.values():
            user.annot_coins = user.coins
            user.annot_prestige = user.prestige
            user.annot_gain = user.gained
            user.annot_work_gain = user.work_gain
            user.gained = 0
        
        annot_mining_prob = get_mining_prob()
        
        generateStats(users)
        
        if clicked:
            if len(ind["ind"]):
                update_annot(ind)
                update_graph(ind)
                update_node(ind)
    
        new_transfers_edges = []
        new_transfers_nodes = []
        new_transfers_amount = {}
        
        miner = np.random.choice(list(DG), p=get_mining_prob())
        users[miner].coins += misc.block_reward
        
        print('\n[----Block {}---miner:{}---reward: {}-----]'.format(block_id, miner, misc.block_reward))
        
        root_gen = (root_node for root_node in roots if not all(x in traversed for x in DG.successors(root_node)))
        for root_node in root_gen:
            #print('root_node: {}'.format(root_node))
            
            successor_gen = (successor_node for successor_node in DG.successors(root_node) if successor_node not in traversed)
            for successor_node in successor_gen:
                #print('successor_node: {}'.format(successor_node))
                
                if users[successor_node].isWorking() and (users[successor_node].prestige >= misc.f):
                    print('\nNode {} distributes to Node {}'.format(root_node, successor_node))
                    
                    # prestige transfer
                    
                    ### new progressive section ###
                    current_node = copy.copy(successor_node)
                    pred = list(DG.predecessors(current_node))
                    prestige_transit = 0
                    
                    while(pred):
                        path_before = nx.shortest_path(DG, 0, pred[0])
                        path_strength = calculatePathStrengthSum(users, path_before)
                        node_strength = calculateNodeStrength(users, current_node)
                        
                        info = animationCalculateTransfer(current_node, node_strength, path_strength, prestige_transit, traversed)
                        prestige_transit = info['transfer']
                        users[current_node].gained += info['gained'] - info['lost']
                        #users[current_node].prestige += users[current_node].gained
                        
                        sn = current_node
                        rn = pred[0]
                        new_transfers_edges.append((sn, rn))
                        if (sn, rn) in new_transfers_amount:
                            new_transfers_amount[(sn, rn)] += round(prestige_transit, 2)
                        else:
                            new_transfers_amount[(sn, rn)] = round(prestige_transit, 2)
                        
                        #print('{} transfers {} upstream'.format(sn, prestige_transit))
                        
                        current_node = pred[0]
                        pred = list(DG.predecessors(current_node))
                    
                    #print("Adding ", prestige_transit, "to root ", current_node)
                    users[current_node].gained += prestige_transit
                    
                    #new_transfers_edges.append((successor_node, root_node))
                    new_transfers_nodes.append(successor_node)
                    #new_transfers_amount[(successor_node, root_node)] = misc.f
                    
                    edgelist = copy.copy(new_transfers_edges)
                    recently_traversed = copy.copy(new_transfers_nodes)
                    edgeLabelList = copy.copy(new_transfers_amount)
                    
                    traversed.append(successor_node)
        
        roots = copy.copy(traversed)
        
        block_id += 1
        
        for user in users:
            users[user].prestige += users[user].gained
            users[user].work_gain += users[user].gained

        for user in users.values():
            user.update()

def printHelp():
    print("usage ./all_animations <str: mining_type (simple/progrssive)> <int: n_nodes> <bool: show_network_edges>")
    quit()

############################################################################################################################################

##### animations.sh command line options #####
if(len(sys.argv) == 4):
    if sys.argv[1].lower() == 'simple':
        updatefig = simple_mining
        title = 'Simple Mining Simulation'
    
    elif sys.argv[1].lower() == 'progressive':
        updatefig = progressive_mining
        title = 'Progressive Mining Simulation'

    else:
        printHelp()

    n_nodes = int(sys.argv[2])

    if sys.argv[3].lower() == 'true':
        show_network_edges = True

    elif sys.argv[3].lower() == 'false':
        show_network_edges = False

    else:
        printHelp

else:
    printHelp()

###### figure configurations #####
fig = plt.figure(figsize=(16,7.5))

ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(222)
ax3 = fig.add_subplot(247)
ax4 = fig.add_subplot(248)

fig.suptitle(title)
xlim = [0, 1]
ylim = [-1, 0.1]

annot_pos = [0,0.3]
pause_pos = [xlim[0], ylim[0]]

##### Directed Graph and nodes initialisation #####
DG = generateRandomTree(n_nodes) # Graph (network) and users separated
users = generateUsersCoinPrestige(n_nodes, min_coin=20, max_coin=500, max_prestige=0, work_prob=20) #coin, prestige, work_prob(%)

starting_static = getStaticValues() # save intial static value for each user for visualisation

saved_users = copy.deepcopy(users) # Save users for restart: same amount of starting coins as it is otherwise randomised


total_prestige = sum(user.prestige for user in users.values()) # initial calculation of total prestige for mining probability (0)

roots, paths = find_paths(DG) # get the roots and paths for the Directed Graph
traversed = copy.deepcopy(roots) # saves a copy of the roots, mark as traversed

edgelist = [] # initial list of edges that needs to be drawn (recent transfers)
recently_traversed = [] #initial list of nodes that needs to be coloured green (recently traversed)
edgeLabelList= {} # the labels corresponding with the recent transfer edges
miner = None # initial definition of the miner

##### node positions #####
pos = hierarchy_pos(DG, 0)
#pos = nx.random_layout(DG, center=[0,0])
#pos = nx.fruchterman_reingold_layout(DG, k=0.7, scale=1.12)
#pos = nx.spring_layout(DG, k=0.5)
#pos = graphviz_layout(DG)
#pos = nx.nx_agraph.graphviz_layout(DG, prog='dot')
#pos = nx.circular_layout(DG, scale = 2)
#pos = nx.shell_layout(DG, scale = 1.05)

##### initialisation of the rendering #####
static_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getStaticNodeSize(), node_color='w', edgecolors='k', linewidths=0.5, alpha=0.5, ax=ax1)
prestige_nodes = nx.draw_networkx_nodes(DG, pos, node_size=getPrestigeNodeSize(), node_color=getNodeColour(), alpha=0.4, ax=ax1)
node_labels = nx.draw_networkx_labels(DG, pos, font_size=9, ax=ax1)

if show_network_edges:
    static_edges = nx.draw_networkx_edges(DG, pos, alpha=0.1, ax=ax1)
edges = nx.draw_networkx_edges(DG, pos, edgelist=edgelist, ax=ax1)
edge_labels = nx.draw_networkx_edge_labels(DG, pos, edge_labels=edgeLabelList, ax=ax1)

##### initialisation of annotation (ax4) #####
annot = ax4.annotate("", xy=annot_pos, fontsize='medium', fontweight='ultralight', bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)
annot.set_visible(False)

##### overheads #####
ind = None
cont = False
clicked = False
pause = False
auto = False
block_id = 0
counter = -1

###### connects event to canvas #####
cid1 = fig.canvas.mpl_connect("button_press_event", on_click)
cid2 = fig.canvas.mpl_connect("key_press_event", on_press)

##### animation #####
ani = animation.FuncAnimation(fig, updatefig, interval=1000)
plt.show()

