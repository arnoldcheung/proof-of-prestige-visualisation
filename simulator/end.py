import sys
from random import expovariate, gauss, uniform
from user import *
from simulator import *
import matplotlib
import random
import copy
import misc


font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 12}

matplotlib.rc('font', **font)

users_simple_coins = {}
users_simple_prestige = {}

users_prog_coins = {}
users_prog_prestige = {}
users_stats_counter = -1


def generateGlobalStats(users_simple, users_prog, inc = 1):
    global users_stats_counter
    global local_counter
    users_stats_counter += inc
    print("Increasing counter to ", users_stats_counter)
    #counter to distinguish between simple and progressive mining

    if(users_stats_counter == 0):
        for user in users_simple:
            users_simple_coins[user] = list()
            users_simple_prestige[user] = list()


        for user in users_prog:
            users_prog_coins[user] = list()
            users_prog_prestige[user] = list()



    for user in users_simple:
        users_simple_coins[user].append((users_stats_counter, users_simple[user].coins))
        users_simple_prestige[user].append((users_stats_counter, users_simple[user].prestige))# -users_simple[user].staticValue()))

    for user in users_prog:
        users_prog_coins[user].append((users_stats_counter, users_prog[user].coins))
        users_prog_prestige[user].append((users_stats_counter, users_prog[user].prestige))# - users_prog[user].staticValue()))

stats = {}
stats['node'] = list()
stats['d'] = list()
stats['coins'] = list()
stats['prestige'] = list()
stats['type'] = list()
stats['iter'] = list()
iter_counter = 0

def collectGlobalStats(users_simple, users_prog):
    global users_simple_prestige, users_prog_prestige, users_stats_counter

    print("users_simple_prestige", users_simple_prestige)
    print("users_prog_prestige", users_prog_prestige)
    for user in users_simple:
        stats['node'].append(users_simple[user].id)
        stats['d'].append(users_simple[user].d)
        stats['coins'].append(users_simple[user].coins)
        stats['prestige'].append(sum(list([i[1] for i in users_simple_prestige[user]])))
        #stats['prestige'].append(sum(users_simple_prestige[user][1]))
        stats['type'].append('simple')
        stats['iter'].append(iter_counter)

    for user in users_prog:
        stats['node'].append(users_prog[user].id)
        stats['d'].append(users_prog[user].d)
        stats['coins'].append(users_simple[user].coins)
        stats['prestige'].append(sum(list([i[1] for i in users_prog_prestige[user]])))
        #stats['prestige'].append(sum(users_prog_prestige[user][1]))
        stats['type'].append('prog')
        stats['iter'].append(iter_counter)




def printCollectedStats():
    print(stats)

    df = pd.DataFrame(stats)
    fig, ax = plt.subplots()
    for key, group in df.groupby('node'):
        print("group ", group)
        #bx = group.plot('d', 'prestige', style='o', ax=ax, legend=True);
        #bx.set_xlabel("d parameter")
        #bx.set_ylabel("Prestige gained")

        avg = group.groupby('d')['prestige'].mean()
        std = group.groupby('d')['prestige'].std()
        ax.set_xlabel("ax label")
        #if(group.get_level_values('type') == 'simple'):
        styles = ['solid', 'dashed', 'dashdot', 'dotted']
        bx = avg.plot(x='d', y='prestige', ax=ax, legend=True, linewidth=2.2, linestyle=styles[key]);
        #else:
        #    bx = avg.plot(x='d', y='prestige', yerr=std, ax=ax, legend=True, linestyle='dashed');
        bx.set_xlabel("d")
        bx.set_ylabel("Prestige gained")
        bx.set_yscale('log')

    plt.legend(labels=['Simple, C=50, W=5', 'Simple, C=10, W=25', 'Prog, C=50, W=5', 'Prog, C=10, W=25'])
    #plt.savefig('foo.pdf')
    plt.show()



def printGlobalStats(users_simple, users_prog):
    global users_stats_counter
    local_counter = 0
    for user in users_simple:
        keys = list([i[0] for i in users_simple_prestige[user]])
        vals = list([i[1] for i in users_simple_prestige[user]])
        user_label = "Simple, C=" + str(users_simple[user].coins)
        plt.plot(keys, vals, label=user_label, linewidth=2.2, linestyle='solid')
        plt.plot((0, users_stats_counter), (users_simple[user].staticValue(), users_simple[user].staticValue()), 'k-', linestyle='dashed')


    for user in users_prog:
        keys = list([i[0] for i in users_prog_prestige[user]])
        vals = list([i[1] for i in users_prog_prestige[user]])
        user_label = "Progressive, C=" + str(users_prog[user].coins)
        plt.plot(keys, vals, label=user_label, linewidth=2.2, linestyle='dashed')
        #plt.plot((0, users_stats_counter), (users[user].staticValue(), users[user].staticValue()), 'k-', linestyle='dashed')


    print("users_simple_coins: ", users_simple_coins)
    print("users_prog_coins: ", users_prog_coins)
    print("users_simple_prestige: ", users_simple_prestige)
    print("users_prog_prestige: ", users_prog_prestige)

    plt.xlabel('Time[blocks]')
    plt.ylabel('Prestige')
    #plt.title("Prestige evolution over time")
    plt.legend()
    plt.show()






def printHelp():
    print("usage ./function <number_of_iterations>")
    quit()


if(len(sys.argv) == 2):
    nIterations = int(sys.argv[1])
    print("Running ",  nIterations, " iterations")
else:
    printHelp()
    quit()



tree_size = 50


for i in range(1, 20):
    for d100 in range(1, 40, 1):
        d = d100 / 100
        misc.f = (100/d) * 0.2

        users_simple = {}
        users_simple[0] = User(0, 50, 0, d=d, work_prob = 5)
        users_simple[1] = User(1, 10, 0, d=d, work_prob = 25)

        users_progressive = {}
        users_progressive[0] = User(2, 50, 0, d=d, work_prob = 5)
        users_progressive[1] = User(3, 10, 0, d=d, work_prob = 25)


        print("our users:")
        for user in users_progressive.values():
            print(user)

        print("~~~~~~~~~~~~~~~~~~~~")
        for i in range(0, nIterations):

            for user in users_simple.values():
                user.update()

            for user in users_progressive.values():
                user.update()

            working_set = set()
            for user in users_simple.values():
                if(user.isWorking() == True):
                   working_set.add(user)




            if (len(working_set) > 0):
                users_tree_orig_simple = generateUsersPrestige(tree_size, users_simple[0].staticValue())
                users_tree_orig_prog = copy.deepcopy(users_tree_orig_simple)
                users_tree_simple = copy.deepcopy(users_tree_orig_simple)
                users_tree_prog = copy.deepcopy(users_tree_orig_prog)
                G = generateRandomTree(tree_size)

                bindings = {}
                for user in working_set:
                    #choose a user to replace

                    bindings[user.id] = random.randint(0, tree_size - 1)
                    print("user ", user.id, " replaces #", bindings[user.id])

                    #increase offset by number of transfers
                    users_simple[user.id].addOffset(G.out_degree(bindings[user.id]))
                    users_progressive[user.id].addOffset(G.out_degree(bindings[user.id]))

                    #replace the corresponding user in simple minig tree
                    users_tree_simple[bindings[user.id]].prestige = users_simple[user.id].prestige
                    users_tree_orig_simple[bindings[user.id]].prestige = users_simple[user.id].prestige
                    users_tree_simple[bindings[user.id]].coins = users_simple[user.id].coins

                    #replace the corresponding user in progressive minig tree
                    users_tree_prog[bindings[user.id]].prestige = users_progressive[user.id].prestige
                    users_tree_orig_prog[bindings[user.id]].prestige = users_progressive[user.id].prestige
                    users_tree_prog[bindings[user.id]].coins = users_progressive[user.id].coins

                #perform prestige transfer simulations for simple and progressive mining
                calculatePrestigeSimple(users_tree_simple, G)
                print("Progressive users")
                for user in users_tree_prog.values():
                    print(user)
                calculatePrestige(users_tree_prog, G)

                for user in working_set:
                    simple_prestige_gain = users_tree_simple[bindings[user.id]].prestige  - users_tree_orig_simple[bindings[user.id]].prestige
                    if(simple_prestige_gain < 0):
                        simple_prestige_gain = 0
                    users_simple[user.id].prestige += simple_prestige_gain

                    prog_prestige_gain = users_tree_prog[bindings[user.id]].prestige  - users_tree_orig_prog[bindings[user.id]].prestige
                    if(prog_prestige_gain < 0):
                        prog_prestige_gain = 0
                    users_progressive[user.id].prestige += prog_prestige_gain




            generateGlobalStats(users_simple, users_progressive)

        collectGlobalStats(users_simple, users_progressive)

        print("Reseting stats")
        users_simple_coins = {}
        users_simple_prestige = {}

        users_prog_coins = {}
        users_prog_prestige = {}
        users_stats_counter = -1


    iter_counter += 1


printCollectedStats()
