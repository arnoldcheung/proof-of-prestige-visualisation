# Proof of Prestige (Animations)

This repository contains implementation of the Proof of Prestige protocol and the animations for visualisation. 

## Simulator
The repository contains a simulator written in python3

### Code
The main implemenetation is located in *simulator.py* and *user.py*. The rest of the files describe just different scenarios used in the paper. 

### Running

#### Ubuntu
Clone the repository:

`git clone git@gitlab.com:fluentic/proof-of-prestige.git`

Install python3 and pip3

`sudo apt install python3`

`sudo apt install python3-pip python3-tk`

Install [networkx](http://pyviz.org/tutorial/06_Network_Graphs.html), pandas, matplotlib

`pip3 install networkx pandas matplotlib`

To recreate all the result presented in the paper, run create_paper_results.sh. Note that the simulation often uses random topologies/starting prestige values, so the result may be slightly different.

`cd simulator`

`bash ./create_paper_results.sh`

Note that the simulation takes a while to complete. The script will display graphs when it's done with a part of the simulatiorn, to continue the simulation, simply close the graphs. 


## Animation section

Everything is in animations.sh file

`bash ./animations.sh`

misc.py contains: amount of prestige transfer, amount of block reward, visualisation node size modifier
